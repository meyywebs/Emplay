# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.mail import send_mail
from django.shortcuts import render
from .forms import CustomForm, SignUpForm
from django.conf import settings
from models import SignUp
from models import Account
from models import AccountRisk
# Create your views here.
def contact (request):
	title = 'Contact Us'
	text_align_center = True
	form = CustomForm(request.POST or None)
	context={
		"form": form,
		"title": title,
		"text_align_center": text_align_center,
	}
	if form.is_valid():
		# for key,value in form.cleaned_data.iteritems():
		# print key,value
		email = form.cleaned_data.get('email')
		full_name = form.cleaned_data.get('full_name')
		message = full_name +' : '+form.cleaned_data.get('message') +' ||for more : '+email
		to_email = ['vishnu.thg@gmail.com']
		send_mail('From contact us', message, settings.EMAIL_HOST_USER, to_email, fail_silently=False)
	return render(request,"custom.html",context)

def home (request):
	title = 'Welcome'

	# if request.user.is_authenticated():
		#title = "Welcome %s" %(request.user)
	# form = SignUpForm(request.POST or None)
	context = {}
	# if form.is_valid():
	# 	form.save()
	# 	# instance = form.save(commit = False)
	# 	# instance.save()
	# 	# print instance.full_name
	# 	context = {
	# 		"template_title": "Thank You..!"
	# 	}
	querysetchildcount = Account.objects.all().count()
	querysetwoncount = Account.objects.filter(stage='won').count()
	querysetpotentialcount = Account.objects.filter(potential='HP').count()
	querysetpipelinecount = Account.objects.filter(pipeline='HP').count()
	if request.user.is_authenticated() and request.user.is_staff:
		queryset = AccountRisk.objects.all().order_by('-updated')

		context = {
			"logged_in_as": "You are logged in as admin",
			"first":querysetchildcount,
			"second":querysetwoncount,
			"third":querysetpotentialcount,
			"forth":querysetpipelinecount,
			"queryset":queryset
		}
		#return render(request,"stock/home.html",context)
	return render(request,"home.html",context)

def display(request,param):
	title = 'dashboard'
	context = {"logged_in_as": "You are logged in as admin",
			"queryset":''}

	if request.user.is_authenticated() and request.user.is_staff:
		acc_id = AccountRisk.objects.get(account_name=str(param))
		querysetchildcount = Account.objects.filter(account_id=acc_id.id).count()
		querysetwoncount = Account.objects.filter(account_id=acc_id.id,stage='won').count()
		querysetpotentialcount = Account.objects.filter(account_id=acc_id.id,potential='HP').count()
		querysetpipelinecount = Account.objects.filter(account_id=acc_id.id,pipeline='HP').count()
		acc_risk = acc_id.account_risk.split('@')
		context = {"logged_in_as": "You are logged in as admin",
					"first":querysetchildcount,
					"second":querysetwoncount,
					"third":querysetpotentialcount,
					"forth":querysetpipelinecount,
					"ac_name":acc_id.account_name,
					"cust_name":acc_id.customer_name,
					"acc_risk":acc_risk,
					}
	return render (request,"display.html",context)
