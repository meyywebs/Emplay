# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class SignUp(models.Model):
	email = models.EmailField()
	full_name = models.CharField(max_length=120,blank=False,null=True)
	timestamp = models.DateTimeField(auto_now_add=True,auto_now=False)
	updated = models.DateTimeField(auto_now_add=False,auto_now=True)

	def __unicode__(self):
		return self.email
class AccountRisk(models.Model):
	account_name = models.CharField(max_length=120,blank=False,null=True)
	customer_name = models.CharField(max_length=120,blank=False,null=True)
	account_risk = models.TextField(null=True, blank=True, default='')
	updated = models.DateTimeField(auto_now_add=True,auto_now=False)

	def details(self):
		return {'account name':self.account_name,
				'customer name':self.customer_name,
				'account risk':self.account_risk,
				'updated on':self.updated,
				}
	def __unicode__(self):#def __str__(self):
		return self.account_name

class Account (models.Model):
	"""docstring for Account """
	account_id = models.ForeignKey(AccountRisk)
	account_child_id = models.IntegerField(null=True, blank=False)
	LP = 'LP'
	HP = 'HP'
	won = 'won'
	lost = 'lost'
	choice = ((LP,'LP'),(HP,'HP'),)
	stage_choice = ((won,'won'),(lost,'lost'),)
	potential = models.CharField(max_length=2,choices=choice)
	pipeline = models.CharField(max_length=2,choices=choice)
	stage = models.CharField(max_length=4,choices=stage_choice)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	# def __init__(self, arg):
	# 	super(AccountRisk , self).__init__()
	# 	self.arg = arg

	def __unicode__(self):#def __str__(self):
		opening_str = str(self.account_id)+str(self.account_child_id)
		return opening_str