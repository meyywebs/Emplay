from django import forms
from .models import SignUp
import re

class CustomForm(forms.Form):
	full_name = forms.CharField()
	email = forms.EmailField()
	message  = forms.CharField()

class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields = ['full_name','email']

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base,provider = email.split("@")
		domain,extension = provider.split(".")

		if not domain == 'gmail':
			raise forms.ValidationError("accepts only gmail")
		if not extension == 'com':
			raise forms.ValidationError("accepts only .com")

		return email

	def clean_full_name(self):
		full_name = self.cleaned_data.get('full_name')
		name_check = re.compile(r"[^A-Za-zs.]")

		if name_check.search(full_name):
			raise forms.ValidationError("accepts only charactes")

		return full_name